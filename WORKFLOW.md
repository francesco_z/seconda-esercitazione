# Workflow

## Regole sui branch

- Il ramo master è protetto e va usato solo per le release;
- il remo develop è protetto e va usato solo per lo sviluppo;
- ogni issue deve essere svolta in un branch creato appositamente nominato `fix-<numero issue>`, ad esempio `fix-1`. Tali branch sono fork dal ramo develop.

## Regole sulle merge request

- Per condividere le modifiche create un'apposita merge request verso il branch develop con un nome significativo e nel testo il tag `close #<numero issue>` e una breve lista delle modifiche fatte;
- solo le merge request che non segnalano conflitti potranno essere incluse nel ramo develop; qualora ce ne fossero dovrete sistemarle di conseguenza e aggiornare il branch remoto;
- le merge request devono indicare come approvers almeno uno dei seguenti username:
  - BottCode
  - bertof
  - MarcoPasqualini
  - Mou95
- le merge request devono indicare l'obbligo di un'approvazione da un approver.

## Regole sui commit

- I titoli dei commit devono essere più **descrittivi** e brevi possibile;
- i messaggi devono contenere la lista delle modifiche fatte (in breve);
- non intasate il branch con troppi commit

## Regole sulle issue

- Se vi vengono assegnate issue in comune (due o più persone hanno la stessa issue) uno dei partecipanti deve essere assegnato la verifica.