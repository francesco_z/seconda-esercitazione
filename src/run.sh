#!/bin/bash

# check and cd to script directory
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd $SCRIPTPATH

. venv/bin/activate
FLASK_DEBUG=1 flask run
