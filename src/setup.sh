#!/bin/bash

# check and cd to script directory
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd $SCRIPTPATH

# setup venv
python3 -m pip install --user virtualenv
python3 -m virtualenv venv
. venv/bin/activate

# install flask and all other wanted libraries (pandas & others)
pip install flask pandas
